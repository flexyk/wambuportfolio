<!DOCTYPE html>
<html >
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Wambu Portfolio</title>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <!--Navigation goes here-->
    <?php include("includes/navigation.php") ?>
    
   
    <div class="content-container">
         <div class="container">
             <div class="row">
                  <div class="col-md-6">
                  <div class="panel panel-default">
                      <div class="panel-heading">COMPANY ADDRESS</div>
                      <div class="panel-body">
                         <address> 
                           <strong>CompanyName Inc. </strong><br>
                           1234 Example Street<br>
                           Calgary, Ab<br>
                           T2F 3B3<br>
                           Email <a href="mailto:example@gmail.com">example@gmail.com</a>.<br> 
                          </address>
                      </div>
                    </div>
                
                 </div>
                 <div class="col-md-6">
                  <div class="panel panel-default">
                      <div class="panel-heading">Form Details</div>
                      <div class="panel-body">
                        <p>Name :<?php if(isset($name)){ echo $name; } ?></p>
                        <p>Email Address : <?php if(isset($email)){ echo $email; } ?></p>
                        <p>city : <?php if(isset($city)){ echo $city; } ?></p>
                        <p>country : <?php if(isset($country)){ echo $country; } ?></p>
                        
                        
                      </div>
                    </div>
                
                 </div>
                </div>         
             </div>
         </div>
     </div>       
      <!--Navigation goes here-->
    <?php include("includes/footer.php") ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/script.js"></script>
</body>
</html>
