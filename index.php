<!DOCTYPE html>
<html >
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Wambu Portfolio</title>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/ekko-lightbox.min.css">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
    <!--Navigation goes here-->
    <?php include("includes/navigation.php") ?>
    <!-- Intro Header -->
    <div class="single_slide">
        <div class="slide_text">
            <div class="table">
                <h2>Name lastName</h2>
                <h3>Web Designer</h3>
                <h3>Company Name</h3>
                
            </div>
        </div>
    </div>
     <!-- About Section -->
    <section id="about" class="container content-section text-center">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2">
                <h2>About</h2>
                
                <p>Lorem ipsum dolor sit amet, in sit turpis est hymenaeos et, eu urna pellentesque leo felis ridiculus adipiscing, id vestibulum pellentesque adipiscing praesent, erat diam sollicitudin curae. Euismod in volutpat donec proin, luctus rutrum bibendum in ante non convallis. Diam ullamcorper in nulla diam, magna wisi rhoncus ac lectus wisi, cras vestibulum a sapien sed, nec purus varius eu id. Tristique nec mauris hac quis luctus amet, nostra habitasse varius, mauris odio mi libero ipsum, eget mauris eget wisi vel neque lacus. Venenatis risus ultrices placerat dolor vestibulum, eget sagittis dolor taciti, mauris sit, diam et posuere sed hendrerit.</p>
            </div>
        </div>
    </section>
    
    <!-- Creatives Section -->
    <section id="creatives">
         <div class="container">
             <div class="row">
                <div class="col-md-12">
                     <div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:900px;height:500px;overflow:hidden;">
                        <!-- Loading Screen -->
                        <div data-u="loading" style="position:absolute;top:0px;left:0px;background-color:rgba(0,0,0,0.7);">
                            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                            <div style="position:absolute;display:block;background:url('../img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
                        </div>
                        <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:900px;height:500px;overflow:hidden;">
                            <div>
                                <img data-u="image" src="img/blackwhite-waiste.jpg" />
                            </div>
                            <div>
                                <img data-u="image" src="img/lake.jpg" />
                            </div>
                            <div>
                                <img data-u="image" src="img/project%202.jpg" />
                            </div>
                            <div>
                                <img data-u="image" src="img/project%205.jpg" />
                            </div>
                            
                            <div>
                                <img data-u="image" src="img/project.jpg" />
                            </div>
                            <div>
                                <img data-u="image" src="img/project3.jpg" />
                            </div>
                            <div>
                                <img data-u="image" src="img/project4.jpg" />
                            </div>
                            <div>
                                <img data-u="image" src="img/project6.jpg" />
                            </div>
                            <div>
                                <img data-u="image" src="img/Roses.jpg" />
                            </div>
                            <div>
                                <img data-u="image" src="img/Stairs.jpg" />
                            </div>
                            <div>
                                <img data-u="image" src="img/Street%20walk.jpg" />
                            </div>
                            <div>
                                <img data-u="image" src="img/swatch1.jpg" />
                            </div>
                             <div>
                                <img data-u="image" src="img/swatch2.jpg" />
                            </div>
                             <div>
                                <img data-u="image" src="img/swatch3.jpg" />
                            </div>
                             <div>
                                <img data-u="image" src="img/swatch4.jpg" />
                            </div>
                             <div>
                                <img data-u="image" src="img/swatch5.jpg" />
                            </div>
                            
                             <div>
                                <img data-u="image" src="img/swatch6.jpg" />
                            </div>
                            
                        </div>
                        <!-- Bullet Navigator -->
                        <div data-u="navigator" class="jssorb05" style="bottom:16px;right:16px;" data-autocenter="1">
                            <!-- bullet navigator item prototype -->
                            <div data-u="prototype" style="width:16px;height:16px;"></div>
                        </div>
                        <!-- Arrow Navigator -->
                        <span data-u="arrowleft" class="jssora12l" style="top:0px;left:0px;width:30px;height:46px;" data-autocenter="2"></span>
                        <span data-u="arrowright" class="jssora12r" style="top:0px;right:0px;width:30px;height:46px;" data-autocenter="2"></span>
                     </div>
  
                 </div>
                
                </div>         
             </div>
         </section>
         
         <section id="gallery">
         <div class="container">
             <div class="row">
                <div class="col-md-8 col-md-offset-2">
                     <h1 class="page-header">Photos</h1>
                     <ul class="photos gallery-parent">
                         <li><a href="img/blackwhite-waiste.jpg" data-title="" data-footer="This is image 1" data-toggle="lightbox" data-gallery="mygallery"  data-parent=".gallery-parent" data-hover="tooltip" data-placement="top" title=""><img src="img/blackwhite-waiste.jpg" class="img-thumbnail"></a></li>
                         
                         <li><a href="img/lake.jpg" data-title="" data-footer="This is image 2" data-toggle="lightbox" data-gallery="mygallery" data-parent=".gallery-parent" data-hover="tooltip" data-placement="top" title=""><img src="img/lake.jpg" class="img-thumbnail"></a></li>
                         
                         <li><a href="img/project%202.jpg" data-title="" data-footer="This is image 3" data-toggle="lightbox" data-gallery="mygallery" data-parent=".gallery-parent" data-hover="tooltip" data-placement="top" title=""><img src="img/project%202.jpg" class="img-thumbnail"></a></li>
                         
                         
                         <li><a href="img/project%205.jpg" data-title="" data-footer="This is image 4" data-toggle="lightbox" data-gallery="mygallery" data-parent=".gallery-parent" data-hover="tooltip" data-placement="top" title=""><img src="img/project%205.jpg" class="img-thumbnail"></a></li>
                         
                         
                         <li><a href="img/project.jpg" data-title="" data-footer="This is image 5" data-toggle="lightbox" data-gallery="mygallery" data-parent=".gallery-parent" data-hover="tooltip" data-placement="top" title=""><img src="img/project.jpg" class="img-thumbnail"></a></li>
                         
                         <li><a href="img/project3.jpg" data-title="" data-footer="This is image 6" data-toggle="lightbox" data-gallery="mygallery" data-parent=".gallery-parent" data-hover="tooltip" data-placement="top" title=""><img src="img/project3.jpg" class="img-thumbnail"></a></li>
                         
                          <li><a href="img/project4.jpg" data-title="" data-footer="This is image 6" data-toggle="lightbox" data-gallery="mygallery" data-parent=".gallery-parent" data-hover="tooltip" data-placement="top" title=""><img src="img/project4.jpg" class="img-thumbnail"></a></li>
                          
                       <li><a href="img/project6.jpg" data-title="" data-footer="This is image 6" data-toggle="lightbox" data-gallery="mygallery" data-parent=".gallery-parent" data-hover="tooltip" data-placement="top" title=""><img src="img/project6.jpg" class="img-thumbnail"></a></li>

                        <li><a href="img/Roses.jpg" data-title="" data-footer="This is image 6" data-toggle="lightbox" data-gallery="mygallery" data-parent=".gallery-parent" data-hover="tooltip" data-placement="top" title=""><img src="img/Roses.jpg" class="img-thumbnail"></a></li>
                        
                    <li><a href="img/Stairs.jpg" data-title="" data-footer="This is image 1" data-toggle="lightbox" data-gallery="mygallery"  data-parent=".gallery-parent" data-hover="tooltip" data-placement="top" title=""><img src="img/Stairs.jpg" class="img-thumbnail"></a></li>
                         
                         <li><a href="img/Street%20walk.jpg" data-title="" data-footer="This is image 2" data-toggle="lightbox" data-gallery="mygallery" data-parent=".gallery-parent" data-hover="tooltip" data-placement="top" title=""><img src="img/Street%20walk.jpg" class="img-thumbnail"></a></li>
                         
                         <li><a href="img/swatch1.jpg" data-title="" data-footer="This is image 3" data-toggle="lightbox" data-gallery="mygallery" data-parent=".gallery-parent" data-hover="tooltip" data-placement="top" title=""><img src="img/swatch1.jpg" class="img-thumbnail"></a></li> 
                         
                          <li><a href="img/swatch2.jpg" data-title="" data-footer="This is image 3" data-toggle="lightbox" data-gallery="mygallery" data-parent=".gallery-parent" data-hover="tooltip" data-placement="top" title=""><img src="img/swatch2.jpg" class="img-thumbnail"></a></li>
                         
                         
                          <li><a href="img/swatch3.jpg" data-title="" data-footer="This is image 3" data-toggle="lightbox" data-gallery="mygallery" data-parent=".gallery-parent" data-hover="tooltip" data-placement="top" title=""><img src="img/swatch3.jpg" class="img-thumbnail"></a></li> 
                         
                          <li><a href="img/swatch4.jpg" data-title="" data-footer="This is image 3" data-toggle="lightbox" data-gallery="mygallery" data-parent=".gallery-parent" data-hover="tooltip" data-placement="top" title=""><img src="img/swatch4.jpg" class="img-thumbnail"></a></li>
                         
                          <li><a href="img/swatch5.jpg" data-title="" data-footer="This is image 3" data-toggle="lightbox" data-gallery="mygallery" data-parent=".gallery-parent" data-hover="tooltip" data-placement="top" title=""><img src="img/swatch5.jpg" class="img-thumbnail"></a></li> 
                         
                          <li><a href="img/swatch6.jpg" data-title="" data-footer="This is image 3" data-toggle="lightbox" data-gallery="mygallery" data-parent=".gallery-parent" data-hover="tooltip" data-placement="top" title=""><img src="img/swatch6.jpg" class="img-thumbnail"></a></li>
                         
                     </ul>

               
  
                 </div>
                
                </div>         
             </div>
         </section>
         
         <section id="contact">
             
              <?php

                 if(isset($_POST["submit"])){

                     $name=$_POST['name'];
                     $email=$_POST['email'];
                     $content=$_POST['content'];


                 }




                ?>


         <div class="container">
             <div class="row">
                 <div class="col-md-6">
                  <div class="panel panel-default">
                      <div class="panel-heading">SEND MESSAGE</div>
                      <div class="panel-body">
                       
                        <form action="" class="" method="post" >
                          <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name ="name" placeholder="Name">
                          </div>
                          <div class="form-group">
                            <label for="name">Email address</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                          </div>
                          <div class="form-group">
                            <label for="content">Message:</label>
                            
                            <textarea name="content" id="content" cols="30" rows="5" class="form-control"  placeholder=""></textarea>
                          </div>
                           
                         
                          <button type="submit" class="btn btn-default" name="submit">Submit</button>
                        </form>
                        
                        
                        
                      </div>
                    </div>
                
                 </div>
                 <div class="col-md-6">
                  <div class="panel panel-default">
                      <div class="panel-heading">Message Details</div>
                      <div class="panel-body">
                        <p>Name :<?php if(isset($name)){ echo $name; } ?></p>
                        <p>Email Address : <?php if(isset($email)){ echo $email; } ?></p>
                        <p>Message : <?php if(isset($content)){ echo $content; } ?></p>
                      
                      </div>
                    </div>
                
                 </div>
                 
                 <div class="row">
                 <div class="col-md-6">
                  <div class="panel panel-default">
                      <div class="panel-heading">Company Address</div>
                      <div class="panel-body">
                         <address> 
                           <strong>CompanyName Inc. </strong><br>
                           1234 Example Street<br>
                           Calgary, Ab<br>
                           T2F 3B3<br>
                           Email <a href="mailto:example@gmail.com">example@gmail.com</a>.<br> 
                          </address>
                      </div>
                    </div>
                
                 </div>
                </div>    
                 
                </div>         
             </div>
         </div>
     </div>       
             
             
         </section>
         
         
     </div>       
      <!--footer goes here-->
    <?php include("includes/footer.php") ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
   <script src="js/jssor.slider-23.1.1.mini.js"></script>
  <script src="js/ekko-lightbox.min.js"></script>  
   <script type="text/javascript">
        jQuery(document).ready(function ($) {

            var jssor_1_SlideshowTransitions = [
              {$Duration:1200,$Opacity:2}
            ];

            var jssor_1_options = {
              $AutoPlay: 1,
              $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*responsive code begin*/
            /*remove responsive code if you don't want the slider scales while window resizing*/
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 600);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            /*responsive code end*/
        });
    </script>
     <script>
    $(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
        event.preventDefault();
        return $(this).ekkoLightbox();
    });

    
 </script>      
              
  <script src="js/script.js"></script>
</body>
</html>
