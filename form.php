<!DOCTYPE html>
<html >
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Wambu Portfolio</title>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <!--Navigation goes here-->
    <?php include("includes/navigation.php") ?>
    
    <?php
    
     if(isset($_POST["submit"])){

         $name=$_POST['name'];
         $email=$_POST['email'];
         $city=$_POST['city'];
         $country=$_POST['country'];
     
     
     
     
     }
    
    
    
    
    ?>
    
    <div class="content-container">
         <div class="container">
             <div class="row">
                 <div class="col-md-6">
                  <div class="panel panel-default">
                      <div class="panel-heading">FORM</div>
                      <div class="panel-body">
                       
                        <form action="" class="" method="post" >
                          <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" class="form-control" id="name" name ="name" placeholder="Name">
                          </div>
                          <div class="form-group">
                            <label for="name">Email address</label>
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                          </div>
                          <div class="form-group">
                            <label for="city">City</label>
                            <input type="text" class="form-control" id="city" name="city" placeholder="City">
                          </div>
                           <div class="form-group">
                            <label for="country">City</label>
                            <input type="text" class="form-control" id="country" name="country" placeholder="Country">
                          </div>
                         
                          <button type="submit" class="btn btn-default" name="submit">Submit</button>
                        </form>
                        
                        
                        
                      </div>
                    </div>
                
                 </div>
                 <div class="col-md-6">
                  <div class="panel panel-default">
                      <div class="panel-heading">Form Details</div>
                      <div class="panel-body">
                        <p>Name :<?php if(isset($name)){ echo $name; } ?></p>
                        <p>Email Address : <?php if(isset($email)){ echo $email; } ?></p>
                        <p>city : <?php if(isset($city)){ echo $city; } ?></p>
                        <p>country : <?php if(isset($country)){ echo $country; } ?></p>
                        
                        
                      </div>
                    </div>
                
                 </div>
                </div>         
             </div>
         </div>
     </div>       
      <!--Navigation goes here-->
    <?php include("includes/footer.php") ?>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/script.js"></script>
</body>
</html>
